import java.util.Scanner;

public class main {
    public static void main(String[] args){
        Scanner can = new Scanner(System.in);
        String s = can.nextLine();
        int size = getSize(s);
        char[] inputNumber = getInputNumber(s);
        Number number = new Number(size);
        String[][] numbers = getNumberArray(size, number, inputNumber);

        printNumber(size, number, numbers);
    }

    private static int getSize(String s) {
        int size;
        if(s.split("-s").length>1){
            size = Integer.parseInt(s.split("-s")[1].trim());
        }else{
            size=1;
        }
        return size;
    }

    private static char[] getInputNumber(String input){
        return input.split("-s")[0].trim().toCharArray();
    }

    private static String[][] getNumberArray(int size, Number number, char[] inputNum) {
        String[][] numbers = new String[inputNum.length][3+(2*size)];

        for(int iter=0;iter<inputNum.length;iter++){
            numbers[iter] = number.getNumbers().get(Integer.parseInt(String.valueOf(inputNum[iter])));
        }
        return numbers;
    }

    private static void printNumber(int size, Number number, String[][] numbers) {
        for(int printIter=0;printIter<5;printIter++){
            for(String[] row : numbers){
                System.out.print(row[printIter]);
            }
            System.out.print("\n");
            if(size > 1 && printIter%2!=0){
                printIter-=1;
                size-=1;
            }else{
                size = number.getSize();
            }
        }
    }
}
