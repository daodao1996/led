import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Number {

    /*private String[] numbers = new String[]{" - \n| |\n   \n| |\n - ","   \n  |\n   \n  |\n   "," - \n  |\n - \n|  \n - ",
            " - \n  |\n - \n  |\n - ","   \n| |\n - \n  |\n   "," - \n|  \n - \n  |\n - ",
            " - \n|  \n - \n| |\n - "," - \n  |\n   \n  |\n   "," - \n| |\n - \n| |\n - ",
            " - \n| |\n - \n  |\n - "};*/

    private Map<Integer,String[]> numbers = new HashMap<>();
    private int size;

    public Number(int size){
        this.size = size;
        numbers.put(0,new String[]{" "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow(" ")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow("-")+" "});
        numbers.put(1,new String[]{" "+sizeRow(" ")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow(" ")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow(" ")+" "});
        numbers.put(2,new String[]{" "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+" "," "+sizeRow("-")+" "});
        numbers.put(3,new String[]{" "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow("-")+" "});
        numbers.put(4,new String[]{" "+sizeRow(" ")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow(" ")+" "});
        numbers.put(5,new String[]{" "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+" "," "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow("-")+" "});
        numbers.put(6,new String[]{" "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+" "," "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow("-")+" "});
        numbers.put(7,new String[]{" "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow(" ")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow(" ")+" "});
        numbers.put(8,new String[]{" "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow("-")+" "});
        numbers.put(9,new String[]{" "+sizeRow("-")+" ",
                "|"+sizeRow(" ")+"|"," "+sizeRow("-")+" ",
                " "+sizeRow(" ")+"|"," "+sizeRow("-")+" "});
    }


    public Map<Integer, String[]> getNumbers() {
        return numbers;
    }

    public int getSize(){
        return this.size;
    }

    public String sizeRow(String link){
        String res = "";
        for(int iter=0;iter<size;iter++){
            res+=link;
        }
        return res;
    }
}
